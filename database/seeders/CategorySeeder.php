<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Casa', 'Xalet', 'Apartament', 'Pis', 'Masia', 'Garatge'];

        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'name' => strtolower($category),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
